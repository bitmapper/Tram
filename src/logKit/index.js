module.exports = function (requestingModule, tabID, activity, level, message) {
  switch (level) {
    case 'info':
      console.log('[' + new Date().getTime() + '] [INFO] [' + requestingModule + '] [' + tabID + '] [' + activity + '] ' + message)
      break

    case 'warn':
      console.warn('[' + new Date().getTime() + '] [WARN] [' + requestingModule + '] [' + tabID + '] [' + activity + '] ' + message)
      break

    case 'error':
      console.error('[' + new Date().getTime() + '] [EROR] [' + requestingModule + '] [' + tabID + '] [' + activity + '] ' + message)
      break

    default:
      console.error('(If you\'re seeing this, something\'s probably gone very very wrong.) [' + new Date().getTime() + '] [UNKNOWN] [' + requestingModule + '] [' + tabID + '] [' + activity + '] ' + message)
  }
}
