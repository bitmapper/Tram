let requestingModule = 'tabKit.create'
let action = 'Viewport Tab Focus'

module.exports = function (tabID, focus) {
  var tab = document.getElementById(tabID)
  switch (focus) {
    case 'active':
      for (var i = 0; i < document.getElementsByClassName('active').length; i++) {
        document.getElementsByClassName('active')[i].classList.remove('active')
      }
      tab.classList.add('active')
      logKit(requestingModule, tabID, action, 'info', 'Changed viewport focus to this tab')
      break

    case 'background':
      tab.classList.remove('active')
      logKit(requestingModule, tabID, action, 'info', 'Changed viewport focus away from this tab')
      break

    default:
      for (var i = 0; i < document.getElementsByClassName('active').length; i++) {
        document.getElementsByClassName('active')[i].classList.remove('active')
      }
      tab.classList.add('active')
      logKit(requestingModule, tabID, action, 'warn', 'Changed viewport focus to this tab - didn\'t have a specified focus, used active as default')
      break
  }
}
