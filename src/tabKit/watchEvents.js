module.exports = function (tabID) {
  document.getElementById(tabID).addEventListener('new-window', (e) => {
    const protocol = require('url').parse(e.url).protocol
    switch (protocol === 'http:' || protocol === 'https:') {
      case true:
        tabKit.create(e.url, 'active')
        break
    }
  })
  document.getElementById(tabID).addEventListener('enter-html-full-screen', (e) => {
    // I don't know why this needs to be called two times, but it does
    uiKit.fullScreen()
    uiKit.fullScreen()
  })
  document.getElementById(tabID).addEventListener('leave-html-full-screen', (e) => {
    uiKit.fullScreen()
    uiKit.fullScreen()
  })
  document.getElementById(tabID).addEventListener('close', (e) => {
    tabKit.remove(tabID)
  })
}
