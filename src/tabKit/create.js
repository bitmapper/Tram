let requestingModule = 'tabKit.create'
let action = 'Tab Creation'
module.exports = function (url, focus) {
  var tabID = tabKit.genTabID()
  var tab = document.createElement('webview')
  tab.setAttribute('src', url)
  tab.setAttribute('id', tabID)
  document.getElementById('viewport').appendChild(tab)
  tabKit.changeFocus(tabID, focus)
  logKit(requestingModule, tabID, action, 'info', 'Changed viewport focus to this tab')
  tabKit.watchEvents(tabID)
}
