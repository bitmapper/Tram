let requestingModule = 'init'
let tabID = 'N/A'
let activity = 'Startup'
module.exports = function () {
  // define cache for general purpose uses
  global.NodeCache = require('node-cache')
  global.cache = new NodeCache()
  // define cache for keeping track of tab information
  global.NodeCache = require('node-cache')
  global.tabInfoCache = new NodeCache()
  logKit(requestingModule, tabID, activity, 'info', 'Defined caches')

  // populate caches
  var obj = []
  cache.set('tabList', obj)

  // create first tab
  console.warn("This doesn't do much currently. Starting the create function.")
  tabKit.create('./webHub/index.html')
  logKit(requestingModule, tabID, activity, 'info', 'Creating first tab')
}
