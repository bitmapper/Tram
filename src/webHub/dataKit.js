function init () {
  switch (localStorage.getItem('recentSearch1')) {
    case null:
      localStorage.setItem('recentSearch1', '')
  }
  switch (localStorage.getItem('recentSearch2')) {
    case null:
      localStorage.setItem('recentSearch2', '')
  }
  switch (localStorage.getItem('recentSearch3')) {
    case null:
      localStorage.setItem('recentSearch3', '')
  }
}

function addRecentSearch(string) {
  var openSpace
  if (localStorage.getItem('recentSearch1') === '') {
    openSpace = 'recentSearch1'
  } else if (localStorage.getItem('recentSearch2') === '') {
    openSpace = 'recentSearch2'
  } else if (localStorage.getItem('recentSearch3') === '') {
    openSpace = 'recentSearch3'
  } else {
    localStorage.setItem('recentSearch3', localStorage.getItem('recentSearch2'))
    localStorage.setItem('recentSearch2', localStorage.getItem('recentSearch1'))
    openSpace = 'recentSearch1'
  }
  localStorage.setItem(openSpace, string)
}

init()
