// thottery regex for domain name detection
var domainPattern = /^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/

function redirect (url) {
  // test if it's a valid URL or not
  switch (url.substr(0, 3) !== 'htt') {
    case true:
      console.log('redirect says not a valid url')
      url = makeValidURL(document.getElementById('mainText').value)
      break
    case false:
      console.log('redirect says is a valid url')
      break
  }
  // start animation
  document.getElementsByTagName('html')[0].classList.add('outTransition')
  document.getElementsByTagName('body')[0].classList.add('outTransitionBody')
  // set up a meta refresh redirect and attach it to the head
  var redirectTag = document.createElement('meta')
  redirectTag.setAttribute('http-equiv', 'refresh')
  redirectTag.setAttribute('content', '0; url=' + url)
  document.getElementsByTagName('head')[0].appendChild(redirectTag)
}

// if it has just a domain name and/or a slash, we need to stick a protocol in front of it so the webview can load it
function makeValidURL (url) {
  console.log('made ' + url + ' a valid URL')
  return 'http://' + url
}

function search (string) {
  addRecentSearch(string)
  redirect('https://www.ixquick.com/do/search?query=' + string)
}

function checkIfUrl (string) {
  if (/^https?:\/\//.test(string)) {
    string = /^https?:\/\/(.*)/.exec(string)[1]
  }
  switch (string.indexOf('/') > -1) {
    case true:
      console.log('it has a slash')
      string = string.split('/', 1)
      break
    case false:
      console.log('no slash')
      break
  }
  // test if it's a domain name or not
  return domainPattern.test(string)
}

// check for keypress
document.getElementById('mainText').addEventListener('keypress', function (e) {
  var key = e.which || e.keyCode
  switch (key) {
    case 13:
      console.log('key down!')
      switch (checkIfUrl(document.getElementById('mainText').value)) {
        case true:
          console.log('true, redirecting')
          redirect(document.getElementById('mainText').value)
          break
        case false:
          console.log('false, searching')
          search(document.getElementById('mainText').value)
          break
      }
  }
})

document.getElementById('recentSearch1').innerHTML = localStorage.getItem('recentSearch1')
document.getElementById('recentSearch2').innerHTML = localStorage.getItem('recentSearch2')
document.getElementById('recentSearch3').innerHTML = localStorage.getItem('recentSearch3')

document.getElementById('recentSearch1').setAttribute('onclick', 'search(\'' + localStorage.getItem('recentSearch1') + '\')')
document.getElementById('recentSearch2').setAttribute('onclick', 'search(\'' + localStorage.getItem('recentSearch2') + '\')')
document.getElementById('recentSearch3').setAttribute('onclick', 'search(\'' + localStorage.getItem('recentSearch3') + '\')')
