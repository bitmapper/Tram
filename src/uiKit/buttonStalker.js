const remote = require('electron').remote
module.exports = function () {
  document.getElementById('close').addEventListener('click', function (e) {
    var window = remote.getCurrentWindow()
    window.close()
  })

  document.getElementById('maximize').addEventListener('click', function (e) {
    var window = remote.getCurrentWindow()
    switch (window.isMaximized()) {
      case true:
        window.unmaximize()
        break
      case false:
        window.maximize()
        break
    }
  })

  document.getElementById('reload').addEventListener('click', function (e) {
    tabKit.navigation(tabKit.currentActiveTab(), 'reload')
  })

  document.getElementById('back').addEventListener('click', function (e) {
    tabKit.navigation(tabKit.currentActiveTab(), 'goBack')
  })

  document.getElementById('forward').addEventListener('click', function (e) {
    tabKit.navigation(tabKit.currentActiveTab(), 'goForward')
  })

  document.getElementById('home').addEventListener('click', function (e) {
    tabKit.navigation(tabKit.currentActiveTab(), 'loadURL', 'file://' + __dirname + '/../webHub/index.html')
  })

  document.getElementById('tabUp').addEventListener('click', function (e) {
    var tabList = cache.get('tabList')
    if (tabList.indexOf(tabKit.currentActiveTab()) !== 0)  {
      tabKit.changeFocus(cache.get('tabList')[tabList.indexOf(tabKit.currentActiveTab()) - 1], 'active')
    }
  })

  document.getElementById('tabNew').addEventListener('click', function (e) {
    tabKit.create('file://' + __dirname + '/../webHub/index.html', 'active')
  })

  document.getElementById('tabDown').addEventListener('click', function (e) {
    var tabList = cache.get('tabList')
    if (tabList.length !== tabList.indexOf(tabKit.currentActiveTab()) + 1)  {
      tabKit.changeFocus(cache.get('tabList')[tabList.indexOf(tabKit.currentActiveTab()) + 1], 'active')
    }
  })

  document.getElementById('tabClose').addEventListener('click', function (e) {
    var tabList = cache.get('tabList')
    var tab = tabList.indexOf(tabKit.currentActiveTab())
      if (tabList.length > 1) {
        tabKit.remove(tabKit.currentActiveTab())
        tabKit.changeFocus(cache.get('tabList')[tabKit.getNearestTab(tab)], 'active')
      } else {
        if (tabList.length === 1) {
          tabKit.remove(tabKit.currentActiveTab())
          tabKit.create('file://' + __dirname + '/../webHub/index.html', 'active')
        }
      }
  })
}
