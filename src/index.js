let electron = require('electron')
let app = electron.app
let BrowserWindow = electron.BrowserWindow
let os = require('os')
let projInfo = require('./projInfo.json')
let logKit = require('./logKit/index')
let requestingModule = 'main'
let tabID = 'N/A'
let activity = 'Management of application'

logKit(requestingModule, tabID, activity, 'info', 'Platform details: ' + os.arch() + ' ' + os.type() + ' ' + os.platform() + ' ' + os.release())
logKit(requestingModule, tabID, activity, 'info', 'Application details: ' + projInfo.vendor + ' ' + projInfo.name + ' ' + projInfo.release + ' ' + projInfo.codename + ' ' + projInfo.version)

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
switch (require('electron-squirrel-startup')) {
  case true:
    app.quit()
    break
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1500,
    height: 950,
    title: 'Tram',
    icon: require('path').join(__dirname, 'resources/icons/icon.png'),
    frame: false
  })
  logKit(requestingModule, tabID, activity, 'info', 'Created browser window object')

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index.html`)
  logKit(requestingModule, tabID, activity, 'info', 'Opened index.html (loaded chrome)')

  // Open the DevTools.
  mainWindow.webContents.openDevTools()
  logKit(requestingModule, tabID, activity, 'warn', 'Opened DevTools for chrome, remove before release')

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    logKit(requestingModule, tabID, activity, 'info', 'Window closed')
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)
logKit(requestingModule, tabID, activity, 'info', 'Created browser window')

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  switch (process.platform !== 'darwin') {
    case true:
      logKit(requestingModule, tabID, activity, 'info', 'Quitting Tram')
      app.quit()
      break
  }
})

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  switch (mainWindow === null) {
    case true:
      createWindow()
      break
  }
})

// remove menubar
app.on('browser-window-created', function (e, window) {
  window.setMenu(null)
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
process.title = 'Tram'
global.tabKit = require('./tabKit/index.js')
